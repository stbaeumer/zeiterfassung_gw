# Test 01

### Erstellen Sie die Abfrage zu folgendem Sachverhalt: 

1. Eine Tabelle namens ```Schueler``` soll angelegt werden, sofern diese noch nicht existiert. Finden Sie selbst 4 geeignete Attribute mit mindestens drei unterschiedlichen Datentypen, die eine solche Tabelle im Schulverwaltungsprogramm enthalten sollte. Stellen Sie auch eindeutige Identifizierbarkeit des einzelnen Datensatzes her!

#
#
#
#

[//]: # (CREATE TABLE)
[//]: # (IF NOT EXISTS)
[//]: # (schueler(nummer INT,)
[//]: # (angelegt TIMESTAMP,)
[//]: # (geburtsdatum DATETIME,)
[//]: # (nachname VARCHAR50)
[//]: # (PRIMARY KEYnummer);)
[//]: # (This may be the most platform independent comment)

2. Fügen Sie mit ```INSERT``` in Ihre Tabelle einen geeigneten Datensatz mit Werten Ihrer Wahl ein:

#
#
#


### Sie sehen zwei unabhängige Anweisungen. Geben Sie die Voraussetzungen für ein ordnungsgemäßes Funktionieren und sowie die Wirkungsweise folgender Anweisungen: 

1. ```var xyz = require('xyz')```
#
#
#
#
#
2. ```if (err) throw err```
#
#
#
#
#

### Geben Sie im Rumpf beider ```console.log()```-Anweisungen geeignete Rückmeldung an den Benutzer, die jeweils den aktuellen Zustand des Programms treffend beschreibt.

```
app.listen(3000, function() {

    console.log('__________________________________________________________')
    
    if (err) throw err

    console.log('__________________________________________________________')
})

```

### Was bedeutet es, wenn man sagt: *"Der localhost lauscht auf Port 3000."*?




#
#
#
#
#
#
#

### Woran erkennen Sie Funktionen?
#
#

### Ist ```.connect()``` in der Anweisung ```con.connect()``` eher ein Funktionsaufruf oder ein Methodenaufruf? Begründen Sie!
#
#
#
### Was ist das Besondere an einer *ereignisgesteuerten Sprache* wie Javascript?

#
#
#
#

### Was ist ```con```? Und was muss *vor* dem Aufruf von ```con.connect()``` erfolgt sein?

#
#
#
#

### Zu der Anweisung ```const xyz = require('xyz')``` bekommen Sie die Fehlermeldung: ```Error: Cannot find module 'xyz'```. `
1. Welche Ursache kann dem Fehler zugrunde liegen? 

#
#
#

2. Welchen Befehl im Terminal müssen Sie möglicherweise ausführen?

#
#
