## Projektidee

- [Demo](https://crud-express-mongo.herokuapp.com)
- [Tutorial](https://zellwk.com/blog/crud-express-mongodb/)

## Installation

1. Clone repo
2. run `npm install` 

## Usage 

1. run `npm run dev`
2. Navigate to `localhost:3000`
3. Have fun ;)


## HEROKU
https://www.youtube.com/watch?v=6Fu39V6T_G0

1. `heroku login`
2. `heroku create`
3. `git add .`
4. `git commit -m "zeiterfassung"`
5. `git push heroku master`
6. `heroku open`

### Nach einem Quelltextupdate

1. `git add .`
2. `git commit -m "..."`
3. `git push heroku master`
4. Das Wolke- / Kreissymbol in VSC unten links klicken, um mit Bitbucket zu syncen.


# DOKU

# FAQ

Problem: `Error: Cannot find module 'express'`

Lösung: In der Konsole (Strg+ö) eingeben: `$ npm install express --save`

Problem: Nodemon starten

Lösung: Im Terminal eingeben: ```./node_modules/.bin/nodemon server.js```


