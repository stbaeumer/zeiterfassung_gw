// Der Bodyparser bereitet die Daten aus dem HTML-Formular vor der Übergabe an die server.js auf.
// Dazu schlüsselt er den Datenstrom auf und bildet daraus das body-Objekt, das dann über
// req.body abgefragt werden kann. Dieser Middleware-Parser wird bei POST-requests benötigt.
// Der Bodyparser wird im Terminal installiert: 
//  npm install body-parser --save

var bodyParser = require('body-parser')

// Immer dann, wenn Daten dauerhaft gespeichert werden sollen, bedarf es einer Datenbank.
// Die meisten Datenbanken verwenden die Abfragesprache SQL.
// Wir wollen die Datenbank des Herstellers MySQL verwenden.
// MySQL muss in node installiert werden: npm install mysql

// Das Modul mysql wird eingebunden

var mysql = require('mysql')

// Die Verbindungsdaten werden initialisiert. Das Connection-Objekt nimmt alle Anmeldedaten entgegen und
// bietet nach dem Verbindungsaufbau (con.connect) eine Methoden (.query()) zum Schreiben und Lesen der Datenbank.

var con = mysql.createConnection({
    host: "130.255.124.99",
    user: "schueler01",
    password: "schueler01",
    database: "db01"
})

// Eine Verbindung zur Datenbank wird mit den übergebenen Anmeldedaten durch Aufruf der Methode connect() auf das con-Objekt aufgebaut.
// Die Methode connect() nimmt einen Parameter in den runden Klammern entgegen.
// Der übergebene Parameter ist seinerseites eine Funktion, die auch wieder einen Parameter entgegennimmt.
// Die geschachtelte Funktion function(err) nennt man Callback-Funktion. Diese Callback-Funktion kommt erst dann
// zur Ausführung, wenn con.connect() entweder err = true oder err = false zurückgemeldet hat.
// Sobald con.connect() den Value von err zurückgibt, wird err als Parameter in die Callback-Funktion gegeben,
// die dann den Wert auswertet.
// Der Sinn der Callback-Function ist also, dass hier Verarbeitungsschritte erst begonnen werden, wenn ein vorheriger 
// Schritt abgearbeitet wurde.
// Würde connect() ohne Callback-Function als Parameter aufgerufen, dann ist die Reihenfolge der Verarbeitung nicht gesichert.
// Es könnte dann passieren, dass die Datenbank bereits abgefragt wird (query()), obwohl die Verbindung (noch) nicht steht. 
// Gute Erklärung dazu: https://codeburst.io/javascript-what-the-heck-is-a-callback-aba4da2deced

con.connect(function(err){

    // Falls ein Verbindungsfehler auftritt, wird ein Fehler geworfen.

    if (err) throw err

    // Ansonsten wird der Erfolg geloggt:

    console.log("Erfolgreich mit der Datenbank verbunden.")

    // Wenn die Verbindung zur Datenbank steht, wird eine Tabelle in der Datenbank angelegt, 
    // sofern diese Tabelle noch nicht existiert. Jede Datenbank enthält typischerweise sehr viele Tabellen,
    // wobei jede Tabelle Einträge zu _einem_ Sachverhalt mit all seinen relevanten Eigenschaften enthält.
    // Das connection-Objekt ist das Bindeglied zwischen Datenbank und node-Webserver.

    con.query("CREATE TABLE IF NOT EXISTS taetigkeit(nummer INT AUTO_INCREMENT, von TIMESTAMP, bis DATETIME, beschreibung VARCHAR(50), PRIMARY KEY(nummer));", function (err, result) {
        
        if (err) throw err;
        
        // Falls kein Fehler auftritt, wird der Erfolg geloggt.
        
        console.log("Tabelle 'taetigkeit' erfolgreich angelegt, bzw. schon vorhanden.");
    })
})

// Das bereits mit npm installierte Express Framework wird eingebunden.
// Installation von express im Terminal: npm install express

const express = require('express')

// Das app-Objekt wird von Express erzeugt. 
// Das app-Objekt repräsentiert den Server mit all seinen Einstellungen und Funktionen.
// Auf das app-Objekt werden im Folgenden die Express-Methoden aufgerufen.
// Das app-Objekt ist das Bindeglied zwischen node-Webserver und Client-Browser.

const app = express()

// Unterhalb der Instanziierung der Express-App muss EJS eingebunden werden.
// Der Sinn von EJS ist, dass die Daten aus der 
// Datenbank an das HTML-Formular übergeben und tabellarisch
// dargestellt werden können.

app.set('view engine', 'ejs')

// Unterhalb der Instanziierung der Express-App muss der Body-Parser
// in die app eingebunden werden.

app.use(bodyParser.urlencoded({extended: true}))

// Dem Server wird mitgeteilt, in welchem Ordner die statischen Inhalte liegen.
// Beispielsweise ist die Datei, die das Aussehen der App beschreibt (styles.css) dort abzulegen. 
// Statische Dateien sind Dateien, die keine Programmlogik enthalten.

app.use(express.static('public'))

// Wenn man von 'Server' spricht, dann kann zum einen ein Computer gemeint sein. 
// So ein Computer hat einen eindeutigen Namen und eine eindeutige IP-Adresse, unter der er vom Client-Computer erreicht werden kann.
// Ein Server in unserem Sinne ist ein Programm, das auf einem Computer ausgeführt wird und das einen bestimmten Dienst für Clients zur Verfügung stellt.
// Weil es viele verschiedene Dienste gibt, die zeitgleich auf einem Computer ausgeführt werden können, wird jedem Dienst eine eindeutige Portnummer zugewiesen. 
// Unser Node.js-Server soll z. B. auf Port 3000 laufen. Man sagt dann: 'Der Server lauscht auf Port 3000'. 
// Weil unser Server und der Client (Browser) auf dem selben Computer laufen, kann anstelle des eindeutigen Namens bzw. der eindeutigen IP-Adresse einfach
// 'localhost' in der Browseradresszeile eingegeben werden. Zusammen mit der Portnummer schreibt man dann in die Adresszeile 'localhost:3000',
// um die Webseite vom Server abzurufen.
// Zuvor muss der Server noch gestartet werden, indem im Terminal **node server.js** eingebenen wird.
// Das Terminal kann in VSC durch die Tastenkombination ***Strg+Ö*** geöffnet und geschlossen werden.
// Der Server-Dienst wird beendet mit der Tastenkombination ***Strg+C***.

const server = app.listen(process.env.PORT || 3000, () => {
    console.log('listening on 3000 %s.', server.address().port)
  })

// Express kennt eine Methode namens get, die zwei Parameter entgegennimmt:

// 1. Der erste Parameter ist der Pfad, von dem die Anfrage kommt. '/' ist der Standardpfad.
//    Beispiel: Wenn der Anwender im Browser 'localhost:3000' eingibt, ist der Pfad '/' 
//              Wenn der Anwender im Browser 'localhost:3000/irgendwas' eingibt, ist der Pfad '/irgendwas' 
// 2. Der zweite Parameter ist die Callback-Funktion. Die sagt dem Server, wie er auf eine Anfrage vom Browser (englisch:Request; kurz: req) mit 
//    einer Antwort (englisch: Response; kurz: res) antworten soll.

// Anfragen an den Server können mit verschiedenen Weiterleitungsmethoden an den Server gestellt werden.
// Die zwei wichtigsten sind: 
//  1. GET   : Wird eingesetzt, um Daten abzufragen
//  2. POST  : Wird eingesetzt, um Daten an den Server zu senden, um etwas zu erzeugen oder zu verändern.

app.get('/', function(req, res) {
    
    console.log("app.get")
    
    con.query("SELECT * FROM taetigkeit;", function (err, result) {
        
        if (err) throw err;
        
        // Die Variable result beinhaltet das Rückgabeergebnis der SQL-Abfrage
        // "SELECT * FROM taetigkeit;"
        
        //console.log(result);

        // Die Funktion render bereitet die HTML-Seite vor, indem der result an die HTML-Seite
        // übergeben wird.

        res.render('index.ejs',{
            taetigkeiten:result
        });        
    });     
})


app.get('/admin', function(req, res) {
    
    console.log("Administrationsbereich öffnen")
    
    con.query("SELECT * FROM taetigkeit;", function (err, result) {
        
        if (err) throw err;
                
        res.render('index.ejs',{
            taetigkeiten:result
        });        
    });     
})

// Auch die app.post()-Methode leitet die Anfrage an den definierten Pfad weiter 
// mit der definierten Callback-Funktion.

// Die Funktion app.post() wird abgefeuert, sobald der Button
// auf dem HTML-Formular geklickt wird.

app.post('/delete', function(req, res) {
    
    console.log("Delete geklickt")
    
    // Bei Klick auf den Button werden alle Datensätze in der Tabelle gelöscht.

    con.query("DELETE FROM taetigkeit", function (err, result) {
    
        if (err) throw err;

        console.log("Tabelle taetigkeit vollständig geleert.")
    })

    // Alle Datensätze der Tabelle taetigkeit werden abgefragt.
    // Das Sternchen steht für alle Spalten der Tabelle.

    con.query("SELECT * FROM taetigkeit;", function (err, result) {
        
        if (err) throw err;
        
        // Die Variable result beinhaltet das Rückgabeergebnis der SQL-Abfrage
        // "SELECT * FROM taetigkeit;"
        
        //console.log(result);

        // Die Funktion render bereitet die HTML-Seite vor, indem der result an die HTML-Seite
        // übergeben wird.

        res.render('index.ejs',{
            taetigkeiten:result
        })
    })
})

app.post('/', function(req, res) {
    
    console.log("Anlegen geklickt")

    // Dank des Body-parsers ist es nun möglich, Werte aus dem
    // HTML-Formular an Javascript zu übergeben.
    // Die HTML-Textbox mit dem name='text' wird
    // in Javascript abgefragt mit 'req.body.text'

    console.log("Neue Tätigkeit namens " + req.body.text + " erfolgreich angelegt.");

    // Bei Klick auf den Button wird ein neuer Datensatz in der Tabelle angelegt.

    con.query("INSERT INTO taetigkeit(beschreibung, von) VALUES ('" + req.body.text + "', now());", function (err, result) {
        if (err) throw err;
        
        // Falls kein Fehler auftritt, wird der Erfolg gemeldet.
        
        console.log("Neuer Datensatz erfolgreich angefügt: " + req.body.text);
    });

    // Alle Datensätze der Tabelle taetigkeit werden abgefragt.
    // Das Sternchen steht für alle Spalten der Tabelle.

    con.query("SELECT * FROM taetigkeit;", function (err, result) {
        
        if (err) throw err;
        
        // Die Variable result beinhaltet das Rückgabeergebnis der SQL-Abfrage
        // "SELECT * FROM taetigkeit;"
        
        //console.log(result);

        // Die Funktion render bereitet die HTML-Seite vor, indem der result an die HTML-Seite
        // übergeben wird.

        res.render('index.ejs',{
            taetigkeiten:result
        });       
    });        
});


    
   